. 00-project-variables.sh

cardano-cli query utxo --mainnet --address $SENDER
echo "Specify Collateral + Fees TXIN:"
read TXIN1
echo "Specify Admin TXIN:"
read TXIN2

cardano-cli query utxo --mainnet --address $REFERENCE_CONTRACT_ADDR
echo "Which Contributor Token will you change?"
echo "Choose UTxO"
read CONTRACT_TXIN
echo "Chooose Contributor Asset"
read REFERENCE_ASSET

cardano-cli transaction build \
--babbage-era \
--mainnet \
--tx-in $TXIN1 \
--tx-in $TXIN2 \
--tx-in-collateral $TXIN1 \
--tx-in $CONTRACT_TXIN \
--spending-tx-in-reference $REFERENCE_UTXO_REFERENCE_SCRIPT \
--spending-plutus-script-v2 \
--spending-reference-tx-in-inline-datum-present \
--spending-reference-tx-in-redeemer-file $UPDATE_ACTION \
--tx-out $REFERENCE_VALIDATOR_CONTRACT_ADDR+"1500000 + 1 $REFERENCE_ASSET" \
--tx-out-inline-datum-file $CONTRIBUTOR_DATUM_FILE \
--tx-out $SENDER+"1500000 + 1 $ADMIN_ASSET" \
--change-address $SENDER \
--required-signer $SKEY \
--protocol-params-file protocol.json \
--out-file update-alice-datum.draft

cardano-cli transaction sign \
--signing-key-file $SKEY \
--mainnet \
--tx-body-file update-alice-datum.draft \
--out-file update-alice-datum.signed

cardano-cli transaction submit \
--tx-file update-alice-datum.signed \
--mainnet
