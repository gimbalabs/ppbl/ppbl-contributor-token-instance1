# Updated 2022-12-02

# -- Admin Asset
export ADMIN_ASSET="d16a58d273f8192364d3b105b332984bc3e17fd57f9d9b0011e6c13f.5050424c436f6d706c6574696f6e546f6b656e41646d696e5631"
export ADMIN_MINTING_SCRIPT="<YOUR PATH TO>/ppbl-contributor-token/mainnet/tokens-mainnet/minting-policy/v1/admin-minter.script"
export ADMIN_MINTING_SKEY="<YOUR PATH TO>/ppbl-contributor-token/mainnet/minting-keys/v1/mn-v1-admin.skey"

# -- Reference Validator Contract
export REFERENCE_VALIDATOR_CONTRACT_ADDR="addr1w8ec0nc526t5x3yyj3vw0dxk8h6u5f8ft483p4wa2c2yp9sgj7fux"
export REFERENCE_UTXO_ADDR="addr1qygsn6p33aq936phlrx6usd7mguqnnfea9s9ruwuhqcvlhsdu706suvddaa3zf095hfjgnxfex3xuwut8m4nshyjmxfsjcfewq"
export REFERENCE_UTXO_REFERENCE_SCRIPT="dc01bfe3f94c4150915db5801902dc79a5e3a9cfa952322661e4f4c64fce9f4f#1"
export REFERENCE_VALIDATOR_SCRIPT="<YOUR PATH TO>/ppbl-contributor-token/mainnet/plutus-mainnet/output/mn-v1-reference-validator.plutus"

# -- Contributor Asset
export CONTRIBUTOR_POLICY_ID="4879ae2b0c3dd69864bfa29b13bb8e60712f4df2176f43aaa7b9aa3b"
export CONTRIBUTOR_MINTING_SCRIPT="<YOUR PATH TO>/ppbl-contributor-token/mainnet/tokens-mainnet/minting-policy/v1/contributor-minter.script"
export CONTRIBUTOR_MINTING_SKEY="<YOUR PATH TO>/ppbl-contributor-token/mainnet/minting-keys/v1/mn-v1-contrib.skey"

# -- Contributor Token Naming
export REFERENCE_PREFIX="100"
export CONTRIBUTOR_PREFIX="222"
export CONTRIBUTOR_TOKEN_BASENAME="PPBL2022"

# -- Contributor Token Minting Metadata and initial Datum
export CONTRIBUTOR_MINTING_METADATA="<YOUR PATH TO>/ppbl-contributor-token/mainnet/tokens-mainnet/metadata/contributor-metadata.json"
export CONTRIBUTOR_DATUM_FILE="<YOUR PATH TO>/ppbl-contributor-token/mainnet/tokens-mainnet/datum/ppbl-completion-datum-template.json"

# -- Redeemers
export UPDATE_ACTION="<YOUR PATH TO>/ppbl-contributor-token/mainnet/tokens-mainnet/redeemers/UpdateDatum.json"
export REVOVE_ACTION="<YOUR PATH TO>/ppbl-contributor-token/mainnet/tokens-mainnet/redeemers/RemoveReference.json"

export CARDANO_NODE_SOCKET_PATH=<YOUR PATH TO>/cardano/db/node.socket

cardano-cli query tip --mainnet
cardano-cli query protocol-parameters --mainnet --out-file protocol.json
echo ""
