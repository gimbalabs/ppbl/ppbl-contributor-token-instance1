#!/usr/bin/env bash

# Create a reference UTxO
. 00-project-variables.sh

cardano-cli query utxo --mainnet --address $SENDER
echo "Specify a TXIN for fees:"
read LOVELACE_TXIN

cardano-cli transaction build \
--babbage-era \
--mainnet \
--tx-in $LOVELACE_TXIN \
--tx-out $REFERENCE_UTXO_ADDR+13835100 \
--tx-out-reference-script-file $REFERENCE_VALIDATOR_SCRIPT \
--change-address $SENDER \
--out-file create-reference-utxo.draft \
--protocol-params-file protocol.json

cardano-cli transaction sign \
--tx-body-file create-reference-utxo.draft \
--mainnet \
--signing-key-file $SKEY \
--out-file create-reference-utxo.signed

cardano-cli transaction submit \
--mainnet \
--tx-file create-reference-utxo.signed