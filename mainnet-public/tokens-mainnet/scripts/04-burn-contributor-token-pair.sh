#!/usr/bin/env bash

. 00-project-variables.sh

SENDER=<UTILITY ADDRESS>
SKEY=<PATH TO SIGNING KEY>

cardano-cli query utxo --mainnet --address $SENDER
echo "TXIN for FEES and COLLATERAL?"
read TXINFEES
echo "UTXO with CONTRIBUTOR TOKEN to burn?"
read TXIN_CONTRIBUTOR_TOKEN
echo "Contributor Asset to Burn:"
read CONTRIBUTOR_ASSET

echo "UTXO with REF TOKEN to burn?"
read TXIN_REFERENCE_TOKEN
echo "Reference Asset to Burn:"
read REFERENCE_ASSET

cardano-cli transaction build \
--babbage-era \
--mainnet \
--tx-in $TXINFEES \
--tx-in $TXIN_CONTRIBUTOR_TOKEN \
--tx-in $TXIN_REFERENCE_TOKEN \
--mint "-1 $CONTRIBUTOR_ASSET + -1 $REFERENCE_ASSET" \
--mint-script-file $CONTRIBUTOR_MINTING_SCRIPT \
--change-address $SENDER \
--required-signer $CONTRIBUTOR_MINTING_SKEY \
--protocol-params-file protocol.json \
--out-file burn-contrib-token-pair.draft

cardano-cli transaction sign \
--signing-key-file $SKEY \
--signing-key-file $CONTRIBUTOR_MINTING_SKEY \
--mainnet \
--tx-body-file burn-contrib-token-pair.draft \
--out-file burn-contrib-token-pair.signed

cardano-cli transaction submit \
--tx-file burn-contrib-token-pair.signed \
--mainnet
