#!/usr/bin/env bash

. 00-project-variables.sh

SENDER=<UTILITY ADDRESS>
SKEY=<PATH TO SIGNING KEY>

echo "Enter Contributor Alias:"
read CONTRIB_TOKEN_ALIAS
echo "Enter Contributor Address:"
read CONTRIBUTOR_ADDRESS

REFERENCE_TOKEN_NAME=$REFERENCE_PREFIX$CONTRIBUTOR_TOKEN_BASENAME$CONTRIB_TOKEN_ALIAS
CONTRIBUTOR_TOKEN_NAME=$CONTRIBUTOR_PREFIX$CONTRIBUTOR_TOKEN_BASENAME$CONTRIB_TOKEN_ALIAS

REFERENCE_TOKENHEXSTRING=$(xxd -pu <<< $REFERENCE_TOKEN_NAME)
export REFERENCE_TOKENHEX=${REFERENCE_TOKENHEXSTRING%??}

CONTRIBUTOR_TOKENHEXSTRING=$(xxd -pu <<< $CONTRIBUTOR_TOKEN_NAME)
export CONTRIBUTOR_TOKENHEX=${CONTRIBUTOR_TOKENHEXSTRING%??}

CONTRIBUTOR_ASSET=$CONTRIBUTOR_POLICY_ID.$CONTRIBUTOR_TOKENHEX
REFERENCE_ASSET=$CONTRIBUTOR_POLICY_ID.$REFERENCE_TOKENHEX

echo "Will Mint:"
echo $REFERENCE_TOKEN_NAME
echo $REFERENCE_TOKENHEX
echo $REFERENCE_ASSET
echo ""
echo $CONTRIBUTOR_TOKEN_NAME
echo $CONTRIBUTOR_TOKENHEX
echo $CONTRIBUTOR_ASSET
echo ""

cardano-cli query utxo --mainnet --address $SENDER
echo "Which TXIN?"
read TXIN

cardano-cli transaction build \
--babbage-era \
--mainnet \
--tx-in $TXIN \
--tx-out $CONTRIBUTOR_ADDRESS+"1500000 + 1 $CONTRIBUTOR_ASSET" \
--tx-out $REFERENCE_VALIDATOR_CONTRACT_ADDR+"1500000 + 1 $REFERENCE_ASSET" \
--tx-out-inline-datum-file $CONTRIBUTOR_DATUM_FILE \
--mint "1 $CONTRIBUTOR_ASSET + 1 $REFERENCE_ASSET" \
--mint-script-file $CONTRIBUTOR_MINTING_SCRIPT \
--change-address $SENDER \
--required-signer $CONTRIBUTOR_MINTING_SKEY \
--metadata-json-file $CONTRIBUTOR_MINTING_METADATA \
--protocol-params-file protocol.json \
--out-file mint-contrib-token-pair.draft

cardano-cli transaction sign \
--signing-key-file $SKEY \
--signing-key-file $CONTRIBUTOR_MINTING_SKEY \
--mainnet \
--tx-body-file mint-contrib-token-pair.draft \
--out-file mint-contrib-token-pair.signed

cardano-cli transaction submit \
--tx-file mint-contrib-token-pair.signed \
--mainnet
