#!/usr/bin/env bash

. 00-project-variables.sh

cardano-cli query utxo --mainnet --address $SENDER

echo "Which TXIN?"
read TXIN

cardano-cli transaction build \
--babbage-era \
--mainnet \
--tx-in $TXIN \
--tx-out $SENDER+"1500000 + 1 $ADMIN_ASSET" \
--mint "1 $ADMIN_ASSET" \
--mint-script-file $ADMIN_MINTING_SCRIPT \
--change-address $SENDER \
--required-signer $ADMIN_MINTING_SKEY \
--protocol-params-file protocol.json \
--out-file mint-admin-token.draft

cardano-cli transaction sign \
--signing-key-file $SKEY \
--signing-key-file $ADMIN_MINTING_SKEY \
--mainnet \
--tx-body-file mint-admin-token.draft \
--out-file mint-admin-token.signed

cardano-cli transaction submit \
--tx-file mint-admin-token.signed \
--mainnet
