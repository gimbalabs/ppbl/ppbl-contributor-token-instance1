#!/usr/bin/env bash

. 00-project-variables.sh

SENDER=<UTILITY ADDRESS>
SKEY=<PATH TO SIGNING KEY>

cardano-cli query utxo --mainnet --address $SENDER
echo "TXIN for FEES and COLLATERAL"
read TXINFEES
echo "UTXO WITH ADMIN TOKEN"
echo $ADMIN_ASSET
read TXINADMIN

cardano-cli query utxo --mainnet --address $REFERENCE_VALIDATOR_CONTRACT_ADDR
echo "Which TXIN will you burn?"
read REFERENCE_TOKEN_UTXO_TO_BURN
echo "Reference Asset to Burn:"
read REFERENCE_ASSET

cardano-cli transaction build \
--babbage-era \
--mainnet \
--tx-in $TXINFEES \
--tx-in $TXINADMIN \
--tx-in-collateral $TXINFEES \
--tx-in $REFERENCE_TOKEN_UTXO_TO_BURN \
--spending-tx-in-reference $REFERENCE_UTXO_REFERENCE_SCRIPT \
--spending-plutus-script-v2 \
--spending-reference-tx-in-inline-datum-present \
--spending-reference-tx-in-redeemer-file $REMOVE_ACTION \
--tx-out $SENDER+"1500000 + 1 $ADMIN_ASSET" \
--tx-out $SENDER+"1500000 + 1 $REFERENCE_ASSET" \
--change-address $SENDER \
--required-signer $SKEY \
--protocol-params-file protocol.json \
--out-file remove-ref-token.draft

cardano-cli transaction sign \
--signing-key-file $SKEY \
--mainnet \
--tx-body-file remove-ref-token.draft \
--out-file remove-ref-token.signed

cardano-cli transaction submit \
--tx-file remove-ref-token.signed \
--mainnet
