{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module ContributorToken.Compiler where

import Cardano.Api
import Cardano.Api.Shelley (PlutusScript (..), PlutusScriptV2)
import Codec.Serialise (serialise)
import qualified ContributorToken.ReferenceValidator as R
import ContributorToken.Types
import Data.Aeson
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Short as SBS
import qualified Plutus.V1.Ledger.Scripts
import qualified Plutus.V1.Ledger.Value
import qualified Plutus.V2.Ledger.Api
import qualified Plutus.V2.Ledger.Contexts
import qualified PlutusTx
import PlutusTx.Prelude
import Prelude (FilePath, IO)

-- If we do not import Ledger, then
-- how to replace Ledger.Validator?

writeValidator :: FilePath -> Plutus.V2.Ledger.Api.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV2) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Plutus.V2.Ledger.Api.unValidatorScript

writeTreasuryScript :: IO (Either (FileError ()) ())
writeTreasuryScript =
  writeValidator "output/mn-v1-reference-validator.plutus" $
    R.validator $
      ReferenceParams
        { adminPolicyID = "d16a58d273f8192364d3b105b332984bc3e17fd57f9d9b0011e6c13f",
          contributorPolicyID = "4879ae2b0c3dd69864bfa29b13bb8e60712f4df2176f43aaa7b9aa3b"
        }