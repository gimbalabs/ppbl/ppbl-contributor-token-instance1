{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module ContributorToken.Types where

import GHC.Generics (Generic)
import Plutus.Script.Utils.V1.Typed.Scripts.Validators (DatumType, RedeemerType)
import Plutus.Script.Utils.V2.Typed.Scripts (TypedValidator, ValidatorTypes, mkTypedValidator, mkTypedValidatorParam, mkUntypedValidator, validatorScript)
import Plutus.V1.Ledger.Value
import Plutus.V2.Ledger.Api
import Plutus.V2.Ledger.Contexts
import qualified PlutusTx
import PlutusTx.Prelude hiding (Semigroup (..), unless)
import Prelude (Show (..))
import qualified Prelude as Pr

-- ReferenceParams
data ReferenceParams = ReferenceParams
  { adminPolicyID :: !CurrencySymbol,
    contributorPolicyID :: !CurrencySymbol
  }
  deriving (Pr.Eq, Pr.Ord, Show, Generic)

PlutusTx.makeLift ''ReferenceParams

-- ContributorDatum
-- Todo: Create type MasteryLevels, to use in Map
-- Once MVP is working, add something like "Completed Project Hashes"
data ContributorDatum = ContributorDatum
  { masteryLevels :: Map BuiltinByteString Integer,
    contribCount :: !Integer,
    alias :: !BuiltinByteString
  }

PlutusTx.unstableMakeIsData ''ContributorDatum
PlutusTx.makeLift ''ContributorDatum

-- ReferenceRedeemer
data ReferenceRedeemer = UpdateDatum | RemoveReference
  deriving (Show)

PlutusTx.makeIsDataIndexed ''ReferenceRedeemer [('UpdateDatum, 0), ('RemoveReference, 1)]
PlutusTx.makeLift ''ReferenceRedeemer

data ReferenceTypes

instance ValidatorTypes ReferenceTypes where
  type DatumType ReferenceTypes = ContributorDatum
  type RedeemerType ReferenceTypes = ReferenceRedeemer