# PPBL Contributor Token
## Mainnet Instance #001
December 2022

## Quick Reference
- Admin PolicyID: `d16a58d273f8192364d3b105b332984bc3e17fd57f9d9b0011e6c13f`
- Admin Token Name: `PPBLCompletionTokenAdminV1` (`5050424c436f6d706c6574696f6e546f6b656e41646d696e5631`)
- Contributor PolicyID: `4879ae2b0c3dd69864bfa29b13bb8e60712f4df2176f43aaa7b9aa3b`
- Reference Contract Address: `addr1w8ec0nc526t5x3yyj3vw0dxk8h6u5f8ft483p4wa2c2yp9sgj7fux`
- Reference UTxO: `dc01bfe3f94c4150915db5801902dc79a5e3a9cfa952322661e4f4c64fce9f4f#1`
- Metadata Key: `1618033988749`

---

## Contents:
1. Define Instance Parameters
2. Compile Reference Contract
3. Prepare Minting Script for Token Pairs
4. Create Datum
5. Create Metadata

---

## 1. Define Instance Parameters
Before we can compile the reference contract, we'll need to define two PolicyIDs:
```haskell
data ReferenceParams = ReferenceParams
  { adminPolicyID       :: !CurrencySymbol,
    contributorPolicyID :: !CurrencySymbol
  }
  deriving (Pr.Eq, Pr.Ord, Show, Generic)
```

For this instance, I will create two unique minting scripts. Each minting script will require two signatures:
```JSON
{
  "type": "all",
  "scripts":
  [
    {
      "type": "sig",
      "keyHash": "<Utility Wallet>"
    },
    {
      "type": "sig",
      "keyHash": "<Unique Keys>"
    }
  ]
}
```
One key is unique to each minting script. Key pairs can be created using `cardano-cli address key-gen`. Remember to store private keys securely, especially if you're building for mainnet.

A "Utility Wallet" will be used in both scripts. In the next iteration of this project, we will include additional signers in this script.

Once you define the keys for two reference scripts, use `cardano-cli transaction policyid` to generate a PolicyID for each token. You can paste the Admin PolicyID and Contributor PolicyID in the Quick Reference at the top of this document.

[Learn more about minting policies](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-course-02/-/tree/master/project-02)

## 2. Compile Reference Contract
- Open `/mainnet-public/plutus-mainnet/src/ContributorToken/Compiler.hs` and use the PolicyID's (in Plutus, `CurrencySymbol`s) as `ReferenceParams`.
- Start a `nix-shell` in `plutus-apps` on git branch `v1.0.0-alpha1`
- Run `cabal-repl` and `writeTreasuryScript` to compile the contract
- Use `cardano-cli address build` to create a Contract Address

[Learn more about compiling Plutus Contracts](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-course-02/-/tree/master/project-01)

### Note on ambiguity of the word "Reference":
We are using the word "Reference" in two different ways. In Step 2, we created a Contract Address for our "Reference Validator". This is the address where the Reference Token from each Contributor Token pair will be stored. Only holders of the Admin Token defined in each contract instance will be able to spend UTxOs from this address. (In other words, one must hold an Admin Token in order to manage this reputation system.)

Next, we will create a "Reference UTxO", but in this case we're talking about the PlutusV2 *reference script*. The word is technically accurate in each case, but there can be ambiguity here. If anyone has ideas for how we talk about these references, please share at Live Coding or on Discord.

## 3. Create Reference UTxO
### Quick start:
`/mainnet-public/tokens-mainnet/scripts/100-make-ref-utxo.sh`

### Details:
The reference script for this contract can be sent to any address. In this case, I'll send the reference script to my utility wallet because I expect that we'll update the Reference Validator soon. We can explore our options for having a community-run address for reference UTxOs.

If you are familiar with transaction building, this one should look pretty simple:
```bash
cardano-cli transaction build \
--babbage-era \
--mainnet \
--tx-in $LOVELACE_TXIN \
--tx-out $REFERENCE_UTXO_ADDR+200000 \
--tx-out-reference-script-file $REFERENCE_VALIDATOR_SCRIPT \
--change-address $SENDER \
--out-file create-reference-utxo.draft \
--protocol-params-file protocol.json
```
It's a simple sending transaction with one additional line: `--tx-out-reference-script-file`. If you build this transaction as-is, you will get the error:
```
Minimum UTxO threshold not met for tx output: <address> + 2000000 lovelace
Minimum required UTxO: Lovelace 13835100
```
This is because storing a reference script requires a certain amount of lovelace. Replace `2000000` with the `Minimum required UTxO` in your transaction, and it will build successfully.

> Note the `Reference UTxO` at the top of this file.

## 4. Prepare Minting Script for Token Pairs
Project variables are specified in `/mainnet-public/tokens-mainnet/scripts/00-project-variables.sh`. You should only need to change variables here for all included scripts to work.



## 5. Create Initial Datum
Datum is defined in `/mainnet-public/plutus-mainnet/src/ContributorToken/Types.hs`:
```haskell
data ContributorDatum = ContributorDatum
  { masteryLevels   :: Map BuiltinByteString Integer,
    contribCount    :: !Integer,
    alias           :: !BuiltinByteString
  }
```

A `.json` file is used to put this datum on chain. An example is provided in `/mainnet-public/tokens-mainnet/datum/ppbl-completion-datum-template.json`
```json
{
    "constructor": 0,
    "fields": [
        {
            "map": [
                {
                    "k": {"bytes": "333031"},
                    "v": {"int": 2}
                },
                {
                    "k": {"bytes": "333032"},
                    "v": {"int": 2}
                },
                {
                    "k": {"bytes": "333033"},
                    "v": {"int": 2}
                }
            ]
        },
        {
            "int": 0
        },
        {
            "bytes": "<ALIAS>"
        }
    ]
}
```

## 6. Create Metadata
For the moment, we provide a metadata template in `/mainnet-public/tokens-mainnet/metadata/contributor-metadata.json`. An immediate next step is to automate the creation of this file so that we can dynamically create metadata for each Contributor Token pair, by changing the token names.

- [ ] Good enough: Use a bash script to automate Contributor token minting
- [ ] Better: Use Mesh in a React UI to mint Contributor tokens


## 7. Test Datum Update Script
Review `/mainnet-public/tokens-mainnet/scripts/05-admin-updates-datum.sh` to see how to update the inline datum in a UTxO with a reference token.

## Great work! Now let's see how the front end works!